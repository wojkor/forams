import os
import matplotlib.pyplot as plt
import numpy as np

filename = 'forams.txt'
forams_count = {}
avg_chambers = {}
algae = {}
repr_calls = {}


def run():
    for _ in range(30):
        os.system('python -m pyage.core.bootstrap otwornice.config.forams')


def add_value_to_dict(dict, step, val):
    temp = dict.get(step, [])
    temp.append(float(val))
    dict[step] = temp


def process():
    f = open(filename, 'r')
    for l in f.readlines():
        s = l.strip().split(',')
        step = int(s[0])

        add_value_to_dict(forams_count, step, s[1])
        add_value_to_dict(avg_chambers, step, s[2])
        add_value_to_dict(algae, step, s[3])
        add_value_to_dict(repr_calls, step, s[4])

    f.close()


def plot(dict, ylabel, plotfilename):
    x = sorted(dict.keys())
    y = [np.mean(dict[key]) for key in x]
    std = [np.std(dict[key]) for key in x]

    plt.errorbar(x, y, yerr=std, fmt='-o')
    plt.ylabel(ylabel)
    plt.xlabel("step")
    plt.savefig(plotfilename + '.pdf')
    plt.close()


if __name__ == '__main__':
    run()
    process()
    plot(forams_count, 'forams count', 'forams_count')
    plot(avg_chambers, 'average number of chambers', 'avg_chambers')
    plot(algae, 'algae count', 'algae_count')
    plot(repr_calls, 'number of reproduction calls', 'repr_calls')


