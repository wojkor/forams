from __future__ import print_function
from datetime import datetime
from pyage.core.statistics import Statistics
from pyage.core.inject import Inject
from pyage_forams.solutions.foram import Foram


class StepStatistics(Statistics):
    @Inject("environment")
    def __init__(self, interval=10, filename='forams.txt'):
        super(StepStatistics, self).__init__()
        self.interval = interval
        self.filename = "forams=%s.log" % datetime.now().strftime("%Y%m%d_%H%M%S") if filename is None else filename

    def update(self, step_count, agents):
        if step_count % self.interval == 0:
            with open(self.filename, 'ab') as f:
                entry = self._get_entry(agents, step_count)
                print((",".join(map(str, entry))), file=f)

    def summarize(self, agents):
        pass

    def _get_entry(self, agents, step_count):
        forams_count = len(agents[0].forams)
        entry = [step_count, forams_count,
                 sum(f.chambers for f in agents[0].forams.values()) / float(forams_count) if forams_count > 0 else 0,
                 sum(c.algae for row in self.environment.grid for c in row),
                 Foram._reproduce.called]
        return entry